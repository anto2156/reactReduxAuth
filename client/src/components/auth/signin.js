import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import * as actions from '../../actions';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';


function renderFields({ input, label, type, meta: { touched, error } })  {
    return (
            <div>
                <TextField
                    hintText={label}
                    floatingLabelText={label}
                    type={type}
                    {...input}
                    />
                {touched && error && <div className="error">{error}</div>}
            </div>
        )

}


class Signin extends Component {
    handleFormSubmit({email, password}){
        console.log(email, password)
        // Need to do something to log user
        this.props.signinUser({ email, password })
    }

    renderAlert(){
        if(this.props.errorMessage){
            return (
                <div className="alert alert-danger">
                    <strong>Oops!</strong> {this.props.errorMessage}
                </div>
            )
        }
    }
    
    render(){
        const { handleSubmit } = this.props

        return (
            <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                <Field 
                    stlye={{ width: '300px' }}
                    name="email"
                    type="text"
                    component={renderFields}
                    label="Email"
                />
                <Field 
                    stlye={{ width: '300px' }}
                    name="password"
                    type="password"
                    component={renderFields}
                    label="Password"
                />
                {this.renderAlert()}
                <RaisedButton style={{ marginTop: '30px', width: '300px' }} label="Sign in" type='submit' primary={true}/>
            </form>
        )
    }
}

function mapStateToProps(state) {
    return { errorMessage: state.auth.error }
}

Signin = reduxForm({
    form: 'signin'
})(Signin)

export default connect(mapStateToProps, actions)(Signin)