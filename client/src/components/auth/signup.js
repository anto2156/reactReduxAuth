import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import * as actions from '../../actions';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';


function renderFields({ input, label, type, meta: { touched, error } }) {
    return (
        <div>
            <TextField
                hintText={label}
                floatingLabelText={label}
                type={type}
                {...input}
                />
            {touched && error && <div className="error">{error}</div>}
        </div>
    )
}

class Signup extends Component {

    handleFormSubmit(formProps){
        console.log(actions)
        console.log(this.props)
        // Call action creator to sign up the user
        this.props.signupUser(formProps)
    }

    renderAlert(){
        if(this.props.errorMessage){
            return (
                <div className="alert alert-danger">
                    <strong>Opps!</strong> {this.props.errorMessage}
                </div>
            )
        }
    }
    
    render(){
        const { handleSubmit } = this.props

        return (
            <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                <Field 
                    stlye={{ width: '300px' }}
                    name="email"
                    type="text"
                    component={renderFields}
                    label="Email"
                />
                <Field 
                    stlye={{ width: '300px' }}
                    name="password"
                    type="password"
                    component={renderFields}
                    label="Password"
                />
                <Field 
                    stlye={{ width: '300px' }}
                    name="passwordConfirm"
                    type="password"
                    component={renderFields}
                    label="Confirm Password"
                />
                {this.renderAlert()}
                <RaisedButton style={{ marginTop: '30px', width: '300px' }} label="Sign up" type='submit' primary={true}/>
            </form>
        )
    }
}

function validate(formProps) {
    const errors = {}

    if(!formProps.email){
        errors.email = 'Please enter an email'
    }

    if(!formProps.password){
        errors.password = 'Please enter a password'
    }

    if(!formProps.passwordConfirm){
        errors.passwordConfirm = 'Please confirm password'
    }

    if(formProps.password !== formProps.passwordConfirm){
        errors.password = 'Passwords dont match'
    }

    return errors
}

function mapStateToProps(state) {
    return { errorMessage: state.auth.error }
}

Signup = reduxForm({
    form: 'signup',
    validate
})(Signup)


export default connect(mapStateToProps, actions)(Signup)