import React, { Component } from 'react';
import {Card} from 'material-ui/Card';
import {Tabs, Tab} from 'material-ui/Tabs';
import { Link } from 'react-router';
import AuthCard from './auth/auth-card';
import Signin from './auth/signin';
import Signup from './auth/signup';

export default class Welcome extends Component{
   render(){
       return (
           <div className="welcome">
           <div className="background"></div>
            <h2 className="main-title">React Redux Auth</h2>
            <Card className="auth-card fade-in">
                <Tabs>
                    <Tab label="Sign in">
                        <div className="signin-container">
                            <Signin />
                        </div>
                    </Tab>
                    <Tab label="Sign up">
                        <div className="signup-container">
                            <Signup />
                        </div>
                    </Tab>
                </Tabs>
                {this.props.children}
            </Card>
           </div>
       )
   }
}