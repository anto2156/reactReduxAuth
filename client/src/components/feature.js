import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions/index';
import { Link } from 'react-router';
import RaisedButton from 'material-ui/RaisedButton';


class Feature extends Component {
    static contextTypes = {
        router: React.PropTypes.object
    }

    componentWillMount(){
        this.props.fetchMessage()
    }

    signOut(){
        this.props.signoutUser();
        this.context.router.push('/');
    }

    render(){
        // <Link className="nav-link" to="/signout" >Sign Out</Link>
        return (
            <div className="feature">
                {this.props.message}
                <RaisedButton label="Sign out" onClick={this.signOut.bind(this)} primary={true}/>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return { message: state.auth.message }
}

export default connect(mapStateToProps, actions)(Feature)